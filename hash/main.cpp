#include <iostream>
#include <ctime>
#include <fstream>
#include <sstream>
#include <limits>
#include <cmath>
#include <chrono>
#include <random>
/**
 * Hash tables act as sets, their size is always a power of 2.
 *
 * TT: 4 days
 *
 * compile on linux:
 * (set macros as desired)
 * g++ -O2 -std=c++11 main.cpp -o h
 *
 */


#define SEED 20
#define EXP2(a) (1 << a)
/**
 * universe size is 2^32
 */
#define LOG2OF_UNIVERSE_SIZE 32
/**
 * index size = 8 bits => 256 values per table
 * num tables = 4 => 4 tables (for a 32-bit key)
 * value size 32 bits
 *
 * overall size = 4 * 256 * 32 bits < 256 kB,
 * should fit into L1 Cache of 256 KB
 */
#define NUMBER_OF_TABLES_IN_TABULATION 4



/**
 * Random test:
 *      BENCHMARK BENCHMARK_OPS | BENCHMARK_TIME
 *      HASH_FUNCTION(r) HF_MODULO | HF_MULTIPLY(r) | HF_TABULATION(r)
 *      HASH_TABLE(ls, h1, h2) TAB_LINEAR_PROBE(ls, h1) | TAB_CUCKOO(ls, h1, h2)
 *      RUNNER(ls) RUNNER_RANDOM(ls)
 *
 * Sequential test:
 *      BENCHMARK BENCHMARK_OPS
 *      HASH_FUNCTION(r) HF_MULTIPLY(r) | HF_TABULATION(r)
 *      RUNNER(ls) RUNNER_SEQUENTIAL
 */
#define BENCHMARK_OPS 1
#define BENCHMARK_TIME 2
#define BENCHMARK BENCHMARK_TIME

#define HF_MODULO ModuloHash()
#define HF_MULTIPLY(r) MultiplyShiftHash(r)
#define HF_TABULATION(r) TabulationHash(r)
#define HASH_FUNCTION(r) HF_TABULATION(r)

#define TAB_LINEAR_PROBE(ls, h1) LinearProbeTable(ls, h1)
#define TAB_CUCKOO(ls, h1, h2) CuckooTable(ls, h1, h2)
#define HASH_TABLE(ls, h1, h2) TAB_CUCKOO(ls, h1, h2)

#define RUNNER_RANDOM(ls) RandomRunner(ls)
#define RUNNER_SEQUENTIAL SequentialRunner()
#define RUNNER(ls) RUNNER_RANDOM(ls)



/**
 * HELPER CLASSES -------------------------------------------------------------------------
 */
class Debug {
public:
    static void startTimer()
    {
        startTimeCpu = std::clock();
        startTimeWall = std::chrono::system_clock::now();
    }
    static std::string sToMin(int s)
    {
        int min = s / 60;
        s = s - (60*min);
        return std::to_string(min) + " min " + std::to_string(s) + " s";
    }
    static void waitForKeyPressEnter()
    {
        std::cin.get();
    }
    static void assert(bool condition, const std::string & message)
    {
        if (!condition){
            exit("Assert failed: " + message);
        }
    }
    static void exit(const std::string & message)
    {
        std::cerr << message << std::endl;
        std::exit(1);
    }
    static long getCpuTimeMs()
    {
        clock_t clocksElapsed = std::clock() - startTimeCpu; //s
        long timeElapsed = clocksElapsed * 1000 / CLOCKS_PER_SEC;
        return timeElapsed;
    }
    static long getWallTimeMs()
    {
        std::chrono::duration<double> duration = std::chrono::system_clock::now() - startTimeWall; //s
        return (long)(duration.count() * 1000);
    }
    static long long getWallTimeNs()
    {
        auto finish = std::chrono::high_resolution_clock::now();
        return std::chrono::duration_cast<std::chrono::nanoseconds>(finish-startTimeWall).count();
    }

private:
    static clock_t startTimeCpu;
    static std::chrono::time_point<std::chrono::system_clock> startTimeWall;
};
clock_t Debug::startTimeCpu = 0;
std::chrono::time_point<std::chrono::system_clock> Debug::startTimeWall = std::chrono::system_clock::now();




/**
 * Taken from 2nd homework
 */
class Random {
/**
 * This is the xoroshiro128+ random generator, designed in 2016 by David Blackman
 * and Sebastiano Vigna, distributed under the CC-0 license. For more details,
 * see http://vigna.di.unimi.it/xorshift/.
 */
public:
    Random(uint32_t seed)
    {
        rng_state[0] = seed * 0xdeadbeef;
        rng_state[1] = seed ^ 0xc0de1234;
        for (int i=0; i<100; i++){
            nextU64();
        }
    }
    uint32_t nextU32()
    {
        return (uint32_t)(nextU64() >> 11);
    }
    uint32_t nextU32Odd()
    {
        uint32_t result = nextU32();
        if (result % 2 == 0){
            result ++;
        }
        return result;
    }

private:
    uint64_t rng_state[2];

    uint64_t nextU64()
    {
        uint64_t s0 = rng_state[0], s1 = rng_state[1];
        uint64_t result = s0 + s1;
        s1 ^= s0;
        rng_state[0] = rng_rotl(s0, 55) ^ s1 ^ (s1 << 14);
        rng_state[1] = rng_rotl(s1, 36);
        return result;
    }
    uint64_t rng_rotl(uint64_t x, int k)
    {
        return (x << k) | (x >> (64 - k));
    }
};






/**
 * HASH -------------------------------------------------------------------------
 */

struct nullable_uint32_t {
    uint32_t value;
    bool isNull;
};
struct array_of_nullable_uint32_t {
    nullable_uint32_t * data;
    uint32_t elementCount;
    uint32_t log2of_maxSize;
    uint32_t maxSize;
};
struct tabulation_table_t {
    uint32_t * data;
    uint32_t log2of_size;
};
enum CuckooHash {
    H1 = 1,
    H2 = 2
};
struct cuckoo_element_t {
    uint32_t value;
    CuckooHash nextHashFn;
    bool isNull;
};
struct cuckoo_array_t {
    cuckoo_element_t * data;
    uint32_t elementCount;
    uint32_t log2of_maxSize;
    uint32_t maxSize;
};


class Hash {
/**
 * From [0, 2^32 - 1] to [0, tableSize-1]
 */
public:
    virtual uint32_t hash(uint32_t x, uint32_t log2of_tableSize) = 0;
    virtual void rebuild() = 0;
    virtual ~Hash(){}
protected:
    /**
     *
     * @param value
     * @param bitCount
     * @return lowest bitCount bits of number x
     */
    uint32_t lowestBits(uint32_t value, uint32_t bitCount)
    {
        /**
         * bitmask for k bits = 2^k - 1
         *
         * eg.
         * 2^3       2^3 -1
         * 0001b     111b
         */
        uint32_t bitmask = (uint32_t)(EXP2(bitCount) - 1);
        return value & bitmask;
    }
};

class ModuloHash: public Hash {
public:
    /**
     * @param x
     * @param log2of_tableSize = log2(m)
     * @return x % m
     */
    uint32_t hash(uint32_t x, uint32_t log2of_tableSize) override
    {
        /**
         * If m == 2^k,
         * x % m == lowest k bits of x
         * eg. x % 8 == lowest 3 bits
         *
         * should have better performance than %
         */
        return lowestBits(x, log2of_tableSize);
    }
    void rebuild() override {}
};

class MultiplyShiftHash: public Hash {
public:
    explicit MultiplyShiftHash(Random * rand): rand(rand)
    {
        rebuild();
    }
    /**
     * @param x
     * @param log2of_tableSize = log2(m)
     * @return ((a * x) mod |U|) / (|U| / m), a∈U odd
     */
    uint32_t hash(uint32_t x, uint32_t log2of_tableSize) override
    {
        /**
         * |U|/m = 2^U/2^k = 2^(U-k)
         * Division by 2 is equivalent to downward bit-shift by 1
         */
        return (a * x) >> (LOG2OF_UNIVERSE_SIZE - log2of_tableSize);
        //automatic modulo 2^32 due to overflow
    }
    void rebuild() override
    {
        a = rand->nextU32Odd();
    }

private:
    Random * rand = nullptr;
    uint32_t a = 0;
};

/**
 * The inner hashes of tabulation hash have 32 bits, so the xor'd result is % m.
 */
class TabulationHash: public Hash {
public:
    explicit TabulationHash(Random * rand): rand(rand)
    {
        rebuild();
    }
    ~TabulationHash() override
    {
        deleteTables();
    }
    uint32_t hash(uint32_t x, uint32_t log2of_tableSize) override
    {
        uint32_t bitCount = 0;
        uint32_t hash = 0;
        for (int i = 0; i<tableCount; i++){
            tabulation_table_t table = tables[i];
            uint32_t idx = lowestBits(x >> bitCount, table.log2of_size);
            hash ^= table.data[idx];
            bitCount += table.log2of_size;
        }
        return lowestBits(hash, log2of_tableSize);
    }
    void rebuild() override
    {
        deleteTables();
        tableCount = NUMBER_OF_TABLES_IN_TABULATION;
        uint32_t log2of_tableSize = LOG2OF_UNIVERSE_SIZE/NUMBER_OF_TABLES_IN_TABULATION;
        uint32_t log2of_lastTableSize = LOG2OF_UNIVERSE_SIZE % NUMBER_OF_TABLES_IN_TABULATION;
        tables = new tabulation_table_t[tableCount];
        for (int i = 0; i<tableCount-1; i++){
            tables[i] = createTable(log2of_tableSize);
        }
        if (log2of_lastTableSize == 0){
            tables[tableCount-1] = createTable(log2of_tableSize);
        } else {
            tables[tableCount-1] = createTable(log2of_lastTableSize);
        }
    }

private:
    Random * rand = nullptr;
    uint32_t tableCount = 0;
    tabulation_table_t * tables = nullptr;

    /**
     * Table size is always a power of 2
     * @param bitCount table size is 2^bitCount
     * @return new table filled with random values. the values DO NOT have k bits, they have 32 bits.
     */
    tabulation_table_t createTable(uint32_t log2of_size)
    {
        uint32_t size = (uint32_t)EXP2(log2of_size);
        tabulation_table_t result = { new uint32_t[size], log2of_size };
        for (uint32_t i =0; i<size; i++){
            result.data[i] = rand->nextU32();
        }
        return result;
    }
    void deleteTables()
    {
        for (int i = 0; i<tableCount; i++){
            delete[] tables[i].data;
        }
        delete[] tables;
        tables = nullptr;
    }
};






class HashTable {
public:
    virtual void insert(uint32_t x) = 0;
    virtual double getLoadFactor() = 0;
    double getAvgSteps()
    {
        return (double) steps / ops;
    }
    virtual ~HashTable(){}
    uint32_t getOps()
    {
        return ops;
    }
    void refreshBenchmark()
    {
        steps=0;
        ops=0;
    }

protected:
    uint32_t steps = 0;
    uint32_t ops = 0;
};


class LinearProbeTable: public HashTable {
public:
    LinearProbeTable(uint32_t log2of_size, Hash * hash) : hash(hash)
    {
        uint32_t size = (uint32_t)EXP2(log2of_size);
        table = {new nullable_uint32_t[size], 0, log2of_size, size}; //data, size, log2of_maxSize, maxSize
        for (uint32_t i = 0; i<table.maxSize; i++){
            table.data[i].isNull = true;
        }
    }
    ~LinearProbeTable() override
    {
        delete[] table.data;
        delete hash;
    }
    void insert(uint32_t x) override
    {
        ops++;
        uint32_t idx = hash->hash(x, table.log2of_maxSize);
        while(true){
            steps++;
            if (table.data[idx].isNull){
                table.data[idx].value = x;
                table.data[idx].isNull = false;
                table.elementCount++;
                return;
            } else {
                if (table.data[idx].value == x){ //value already in table
                    return;
                } else {
                    idx = (idx + 1) % table.maxSize;
                    //will never be an infinite loop, because we checked load factor
                }
            }
        }
    }
    double getLoadFactor() override
    {
        return (double)table.elementCount / table.maxSize;
    }

private:
    array_of_nullable_uint32_t table;
    Hash * hash = nullptr;
};


/**
 * Cuckoo hash table checks before each insert whether the key isn't already in the table (2 operations).
 */
class CuckooTable: public HashTable {
public:
    CuckooTable(uint32_t log2of_size, Hash * hash1, Hash * hash2): hash1(hash1), hash2(hash2)
    {
        initializeTable(log2of_size);
    }
    ~CuckooTable() override
    {
        delete[] table.data;
        delete hash1;
        delete hash2;
    }
    void insert(uint32_t key) override
    {
        ops++;
        cuckoo_element_t cuckoo = internal_insert(key);
        if (!cuckoo.isNull){
            int round = 1;
            bool rehashOk = rehash(cuckoo.value);
            while(!rehashOk){
                if (round > 10){
                    std::cerr << "10 consecutive rehashes. exiting." << std::endl;
                    exit(0);
                }
                rehashOk = rehash(cuckoo.value);
                round++;
            }
        }
    }
    double getLoadFactor() override
    {
        return (double)table.elementCount / table.maxSize;
    }

private:
    uint32_t internal_stepCount = 0;
    cuckoo_array_t table;
    Hash * hash1;
    Hash * hash2;


    void print()
    {
        for (int i =0; i<table.maxSize; i++){
            if(table.data[i].isNull){
                std::cerr << "- ";
            } else {
                std::cerr << table.data[i].value << " ";
            }
        }
        std::cerr << std::endl;
    }
    bool isKeyPresent(uint32_t key)
    {
        cuckoo_element_t value1 = table.data[hash(CuckooHash::H1, key)];
        cuckoo_element_t value2 = table.data[hash(CuckooHash::H2, key)];
        return (!value1.isNull && value1.value == key) || (!value2.isNull && value2.value == key);
    }
    /**
     * Hashes the key only if it is not hashed already.
     *
     * @param key
     * @return null or last kicked out element (all others are hashed, but not this one)
     */
    cuckoo_element_t internal_insert(uint32_t key)
    {
        steps -= 3; //2 for checking presence, 1 for the initial hash (is not a swap)
        if (isKeyPresent(key)){
            return {0, CuckooHash ::H1, true};
        }

        uint32_t idx = hash(CuckooHash ::H1, key);
        cuckoo_element_t element = {key, CuckooHash ::H2, false};
        cuckoo_element_t cuckoo = kickOut(idx, element);
        if (cuckoo.isNull){
            table.elementCount++;
            return {0, CuckooHash ::H1, true};
        }

        internal_stepCount = 0;
        uint32_t limit = getStepLimit();
        while (internal_stepCount < limit){
            idx = hash(cuckoo.nextHashFn, cuckoo.value);
            if (cuckoo.nextHashFn == CuckooHash::H1){
                cuckoo.nextHashFn = CuckooHash ::H2;
            } else {
                cuckoo.nextHashFn = CuckooHash ::H1;
            }
            cuckoo = kickOut(idx, cuckoo);
            if (cuckoo.isNull){
                table.elementCount++;
                return {0, CuckooHash ::H1, true};
            }
        }
        //our patience has run out
        return cuckoo;
    }
    uint32_t getStepLimit()
    {
        return (uint32_t)(6 * log2(table.elementCount));
    }
    void initializeTable(uint32_t log2of_size)
    {
        delete[] table.data;
        uint32_t size = (uint32_t)EXP2(log2of_size);
        table = { new cuckoo_element_t[size], 0, log2of_size, size };
        for (uint32_t i = 0; i<table.maxSize; i++){
            table.data[i].isNull = true;
        }
    }
    /**
     * @param additionalKey will be added to the new hash table along with all the values from the old table
     * @return true if rehash successful, false if it takes too long and should be rehashed again
     *      (in that case no change is made to the original hash table and additionalKey)
     */
    bool rehash(uint32_t additionalKey)
    {
        cuckoo_array_t oldTable = table;
        table.data = nullptr;
        initializeTable(oldTable.log2of_maxSize);
        hash1->rebuild();
        hash2->rebuild();
        internal_insert(additionalKey);
        for (uint32_t i = 0; i<oldTable.maxSize; i++){
            cuckoo_element_t data = oldTable.data[i];
            if (!data.isNull){
                cuckoo_element_t unhashedElement = internal_insert(data.value);
                if (!unhashedElement.isNull){
                    delete[] table.data;
                    table = oldTable;
                    return false;
                }
            }
        }
        delete[] oldTable.data;
        return true;
    }
    /**
     *
     * @param idx where to put newElement
     * @param newElement
     * @return value of kicked-out element, null if nothing was kicked out
     */
    cuckoo_element_t kickOut(uint32_t idx, cuckoo_element_t newElement)
    {
        cuckoo_element_t result = table.data[idx];
        table.data[idx] = newElement;
        return result;
    }
    uint32_t hash(CuckooHash hashFn, uint32_t key)
    {
        steps++;
        internal_stepCount++;

        uint32_t idx;
        if (hashFn == CuckooHash::H1){
            idx = hash1->hash(key, table.log2of_maxSize);
        } else { //hashFn == CuckooHash::H2
            idx = hash2->hash(key, table.log2of_maxSize);
        }
        return idx;
    }
};





class RandomRunner {
public:
    Random * rand;
    HashTable * hashTable;

    explicit RandomRunner(uint32_t log2of_tableSize)
    {
        rand = new Random(SEED);
        Hash * hash1 = new HASH_FUNCTION(rand);
        Hash * hash2 = new HASH_FUNCTION(rand);
        hashTable = new HASH_TABLE(log2of_tableSize, hash1, hash2);
    }
    void run()
    {
        double STEP = 0.01;
        for (int percent = 0; percent < 99; percent ++){
            benchmarkInit();
            double initialLoadFactor = percent * STEP;
            double maxLoadFactor = initialLoadFactor + STEP;
            double loadFactor = hashTable->getLoadFactor();
            while(loadFactor < maxLoadFactor){
                hashTable->insert(rand->nextU32());
                loadFactor = hashTable->getLoadFactor();
            }
            benchmarkPrint(initialLoadFactor);
        }
    }
    void benchmarkInit()
    {
        hashTable->refreshBenchmark();
#if BENCHMARK == BENCHMARK_TIME
        Debug::startTimer();
#endif
    }
    void benchmarkPrint(double initialLoadFactor)
    {
#if BENCHMARK == BENCHMARK_OPS
        std::cout << initialLoadFactor << ";" << hashTable->getAvgSteps() << std::endl;
#elif BENCHMARK == BENCHMARK_TIME
        double avgNs = (double)Debug::getWallTimeNs() / hashTable->getOps();
        std::cout << initialLoadFactor << ";" << avgNs << std::endl;
#endif
    }
};



class SequentialRunner {
public:
    Random * rand;
    HashTable * hashTable = nullptr;
    uint32_t num = 0;

    int REPETITIONS = 100;
    double ** results;
    int LOG2OF_MIN_SIZE = 5;
    int LOG2OF_MAX_SIZE = 26;

    explicit SequentialRunner()
    {
        rand = new Random(SEED);
        results = new double * [REPETITIONS];
        for (int rep =0; rep<REPETITIONS; rep++){
            results[rep] = new double[LOG2OF_MAX_SIZE - LOG2OF_MIN_SIZE];
        }
    }
    void run()
    {
        for(int rep = 0; rep<REPETITIONS; rep++){
            for (int log2of_size = LOG2OF_MIN_SIZE; log2of_size < LOG2OF_MAX_SIZE; log2of_size++){
                hashTable = new LinearProbeTable(log2of_size, new HASH_FUNCTION(rand));
                while(hashTable->getLoadFactor() < 0.89)
                {
                    hashTable->insert(num++);
                }
                hashTable->refreshBenchmark();
                while(hashTable->getLoadFactor() < 0.91)
                {
                    hashTable->insert(num++);
                }
                results[rep][log2of_size - LOG2OF_MIN_SIZE] = hashTable->getAvgSteps();
                delete hashTable;
            }
            std::cerr << "Rep done: " << rep << std::endl;
        }

        //write output
        for (int log2of_size = LOG2OF_MIN_SIZE; log2of_size < LOG2OF_MAX_SIZE; log2of_size++) {
            std::cout << EXP2(log2of_size) << std::endl;
        }
        std::cout << std::endl;
        for (int log2of_size = LOG2OF_MIN_SIZE; log2of_size < LOG2OF_MAX_SIZE; log2of_size++){
            for (int rep=0; rep<REPETITIONS; rep++){
                if (rep > 0){
                    std::cout << ";";
                }
                std::cout << results[rep][log2of_size - LOG2OF_MIN_SIZE];
            }
            std::cout << std::endl;
        }
    }
};






int main(int argc, char ** argv)
{
    int log2of_tableSize = 22;
    (new RUNNER(log2of_tableSize))->run();
    exit(0);
}


