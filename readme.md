*This project is public only for presentation purposes. You have no permission to use, modify, or share the software.*

### Technologies ###

C++, R, (Tex, Bash)

### Introduction ###

These are a few complex data structures and algorithms: Splay tree, Fibonacci heap, Cache-friendly matrix transposition,
Cuckoo hashing and Open hashing with linear probe with various hash functions (Modulo, Multiply-shift, Tabulation hash).
Each structure was well tested and achieves the time complexity it is known for.

*Each structure has graph generator (written in R) and an in-depth benchmarking and description (written in Tex).
There may also be a runner (written in Bash)*

*Demo graphs:*

![splay](readme/plot1.png)
![tranposition](readme/plot2.png)
![hashing](readme/plot4.png)


### Deployment ###

Just compile the code on the target platform (tested on Windows with MinGW and Linux with g++).