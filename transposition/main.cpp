#include <iostream>
#include <ctime>
#include <fstream>
#include <sstream>
#include <limits>
#include <math.h>
#include <chrono>


/**
 * If defined, the program just runs tests and quits.
 */
//#define MACRO_TEST 1
/**
 * If defined, generates input for a simulator instead of actually swapping.
 */
//#define MACRO_SIM 1
/**
 * If defined, a trivial algorithm is used instead of the cache-oblivious one.
 */
//#define MACRO_TRIVIAL 1

/**
 * TT: 8h
 *
 * compile on linux:
 * (set macros as desired)
 * g++ -O2 -std=c++11 main.cpp -o t
 *
 */
/**
 * WORTH MENTIONING that simulator input must not get a newline after E.
 */



/**
 * HELPER CLASSES -------------------------------------------------------------------------
 */
class Debug {
public:
    static void startTimer()
    {
        startTimeCpu = std::clock();
        startTimeWall = std::chrono::system_clock::now();
    }
    static std::string sToMin(int s)
    {
        int min = s / 60;
        s = s - (60*min);
        return std::to_string(min) + " min " + std::to_string(s) + " s";
    }
    static void waitForKeyPressEnter()
    {
        std::cin.get();
    }
    static void assert(bool condition, const std::string & message)
    {
        if (!condition){
            exit("Assert failed: " + message);
        }
    }
    static void exit(const std::string & message)
    {
        std::cerr << message << std::endl;
        std::exit(1);
    }
    static long getCpuTimeMs()
    {
        clock_t clocksElapsed = std::clock() - startTimeCpu; //s
        long timeElapsed = clocksElapsed * 1000 / CLOCKS_PER_SEC;
        return timeElapsed;
    }
    static long getWallTimeMs()
    {
        std::chrono::duration<double> duration = std::chrono::system_clock::now() - startTimeWall; //s
        return (long)(duration.count() * 1000);
    }
    static long long getWallTimeNs()
    {
        auto finish = std::chrono::high_resolution_clock::now();
        return std::chrono::duration_cast<std::chrono::nanoseconds>(finish-startTimeWall).count();
    }

private:
    static clock_t startTimeCpu;
    static std::chrono::time_point<std::chrono::system_clock> startTimeWall;
};
clock_t Debug::startTimeCpu = 0;
std::chrono::time_point<std::chrono::system_clock> Debug::startTimeWall = std::chrono::system_clock::now();



struct subMatrix_s {
    long start;
    long width;
    long height;
};
struct slice_s {
    subMatrix_s topLeft;
    subMatrix_s topRight;
    subMatrix_s bottomLeft;
    subMatrix_s bottomRight;

};

/**
 * matrix NxN
 * of int (4B)
 *
 */
class Matrix {
public:
    int * data;
    subMatrix_s matrix;
    int MIN = 50;

    /**
     * @param N number of rows (same as number of cols)
     */
    explicit Matrix(long N)
    {
        matrix = {0, N, N};
        data = new int[N*N]{0};
    }
    /**
     * @param data must be of size N*N
     * @param N
     */
    explicit Matrix(int * data, long N): data(data)
    {
        matrix = {0, N, N};
    }
    ~Matrix()
    {
        delete data;
    }

    /**
     * @param m must be a square matrix
     */
    void transpose(subMatrix_s m)
    {
        for (long colOffset = 0; colOffset < m.width; colOffset++){
            for (long rowOffset = 0; rowOffset < colOffset; rowOffset++){
                long startCol = m.start % matrix.width;
                long startRow = m.start / matrix.width;
                swap(startCol + colOffset, startRow + rowOffset, startCol + rowOffset, startRow + colOffset);
            }
        }
    }
    void transposeAndSwap(subMatrix_s m1, subMatrix_s m2)
    {
        /**
         *
         *               12
         * 111      ->   12
         * 222           12
         */
        for (long colOffset = 0; colOffset < m1.width; colOffset++){
            for (long rowOffset = 0; rowOffset < m1.height; rowOffset++){
                long startCol1 = m1.start % matrix.width;
                long startRow1 = m1.start / matrix.width;
                long startCol2 = m2.start % matrix.width;
                long startRow2 = m2.start / matrix.width;
                swap(startCol1 + colOffset, startRow1 + rowOffset, startCol2 + rowOffset, startRow2 + colOffset);
            }
        }
    }


    void transpose()
    {
#ifdef MACRO_SIM
        std::cout << "N " << matrix.width << std::endl;
#endif

#ifdef MACRO_TRIVIAL
        transpose(matrix);
#else
        transposeRecursive(matrix);
#endif

#ifdef MACRO_SIM
        std::cout << "E";
#endif
    }
    /**
     * @param start inclusive
     * @param end inclusive
     */
    void transposeRecursive(subMatrix_s m)
    {
        if (m.height > MIN){
            slice_s s = slice(m);
            transposeRecursive(s.topLeft);
            transposeRecursive(s.bottomRight);
            transposeAndSwapRecursive(s.bottomLeft, s.topRight);
        } else {
            transpose(m);
        }
    }
    void transposeAndSwapRecursive(subMatrix_s m1, subMatrix_s m2)
    {
        if (m1.width > MIN){slice_s s1 = slice(m1);
            slice_s s2 = slice(m2);
            /**
             *
             * low  hi
             *
             * 1 2  5 6
             * 3 4  7 8
             *
             * 1 <--> 5
             * 4 <--> 8
             * 3 <--> 6
             * 2 <--> 7
             *
             * It doesn't matter which sub-matrix is below the diagonal and which is above
             */
            transposeAndSwap(s1.topLeft, s2.topLeft);
            transposeAndSwap(s1.bottomRight, s2.bottomRight);
            transposeAndSwap(s1.bottomLeft, s2.topRight);
            transposeAndSwap(s1.topRight, s2.bottomLeft);
            /**
             * checking that dimensions of slices fit
             *
             *    3 2         3 3
             *  3           3          yes
             *  3           2
             *
             *    3 2         3 2
             *  3           3          yes
             *  2           2
             *
             */
        } else {
            transposeAndSwap(m1, m2);
        }
    }


    void swap(long row1, long col1, long row2, long col2)
    {
#ifdef MACRO_SIM
        std::cout << "X " << row1 << " " << col1 << " " << row2 << " " << col2 << " " << std::endl;
#else
        long idx1 = row1 * matrix.width + col1;
        long idx2 = row2 * matrix.width + col2;
        int temp = data[idx2];
        data[idx2] = data[idx1];
        data[idx1] = temp;
#endif
    }
    /**
     * @param m it's width can differ from it's height by at most 1
     * @return
     */
    slice_s slice(subMatrix_s m)
    {
        long a = biggerHalf(m.width);
        long b = smallerHalf(m.width);
        long c = biggerHalf(m.height);
        long d = smallerHalf(m.height);
        long N = matrix.width;
        Debug::assert((a == b || a == b+1)&&(c == d || c == d+1), __FUNCTION__);
        /**
         *      a b
         *   c
         *   d
         *
         *   start        |      start + a
         *        ----------------
         *   start + c*N  |      start + c*N + a
         */
         return {
             {m.start, a, c},
             {m.start + a, b, c},
             {m.start + c*N, a, d},
             {m.start + c*N + a, b, d}
         };
    }
    static long smallerHalf(long N)
    {
        return N/2;
    }
    static long biggerHalf(long N)
    {
        return N/2 + N%2;
    }
};


#ifndef MACRO_TEST
/**
 * MAIN -------------------------------------------------------------------------------------------------
 */

int main(int argc, char ** argv)
{
    for (int k = 54; ; k++){
        int N = (long)ceil(pow(2.0, (double)k/9));
        Matrix * m = new Matrix(N);
        Debug::startTimer();
        m->transpose();
        std::cout << N << ";" << Debug::getWallTimeNs()/(N*N) << std::endl;
        delete m;


        if (N == 15170){
            //soon will be out of memory
            break;
        }
    }
    return 0;
}



#else
/**
 * TESTS -----------------------------------------------------------------------------------
 */


void test_transpose_2()
{
    int * data = new int[4]();
    data[0] = 0;
    data[1] = 1;
    data[2] = 2;
    data[3] = 3;
    /**
     * 0 1  --> 0 2
     * 2 3      1 3
     */
    Matrix * m = new Matrix(data, 2);
    m->transposeRecursive(m->matrix);
    Debug::assert(m->data[0] == 0, __FUNCTION__);
    Debug::assert(m->data[1] == 2, __FUNCTION__);
    Debug::assert(m->data[2] == 1, __FUNCTION__);
    Debug::assert(m->data[3] == 3, __FUNCTION__);
}
void test_transpose_3()
{
    int * data = new int[9]();
    data[0] = 0;
    data[1] = 1;
    data[2] = 2;
    data[3] = 3;
    data[4] = 4;
    data[5] = 5;
    data[6] = 6;
    data[7] = 7;
    data[8] = 8;
    /**
     * 0 1 2     0 3 6
     * 3 4 5 --> 1 4 7
     * 6 7 8     2 5 8
     */
    Matrix * m = new Matrix(data, 3);
    m->transposeRecursive(m->matrix);
    Debug::assert(m->data[0] == 0, __FUNCTION__);
    Debug::assert(m->data[1] == 3, __FUNCTION__);
    Debug::assert(m->data[2] == 6, __FUNCTION__);
    Debug::assert(m->data[3] == 1, __FUNCTION__);
    Debug::assert(m->data[4] == 4, __FUNCTION__);
    Debug::assert(m->data[5] == 7, __FUNCTION__);
    Debug::assert(m->data[6] == 2, __FUNCTION__);
    Debug::assert(m->data[7] == 5, __FUNCTION__);
    Debug::assert(m->data[8] == 8, __FUNCTION__);

    //(int[9])*(m->data)
}
void test_transpose_1000()
{
    int * data = new int[1000 * 1000]{0};
    data[780] = 780;

    data[1002] = 6;
    data[2001] = 2001;

    Matrix * m = new Matrix(data, 1000);
    m->transposeRecursive(m->matrix);

    Debug::assert(m->data[780] == 0, __FUNCTION__);
    Debug::assert(m->data[780 * 1000] == 780, __FUNCTION__);
    Debug::assert(m->data[2001] == 6, __FUNCTION__);
    Debug::assert(m->data[1002] == 2001, __FUNCTION__);
}

void test_transposeTrivial_3()
{
    int * data = new int[9]();
    data[0] = 0;
    data[1] = 1;
    data[2] = 2;
    data[3] = 3;
    data[4] = 4;
    data[5] = 5;
    data[6] = 6;
    data[7] = 7;
    data[8] = 8;
    /**
     * 0 1 2     0 3 6
     * 3 4 5 --> 1 4 7
     * 6 7 8     2 5 8
     */
    Matrix * m = new Matrix(data, 3);
    m->transpose(m->matrix);
    Debug::assert(m->data[0] == 0, __FUNCTION__);
    Debug::assert(m->data[1] == 3, __FUNCTION__);
    Debug::assert(m->data[2] == 6, __FUNCTION__);
    Debug::assert(m->data[3] == 1, __FUNCTION__);
    Debug::assert(m->data[4] == 4, __FUNCTION__);
    Debug::assert(m->data[5] == 7, __FUNCTION__);
    Debug::assert(m->data[6] == 2, __FUNCTION__);
    Debug::assert(m->data[7] == 5, __FUNCTION__);
    Debug::assert(m->data[8] == 8, __FUNCTION__);

    //(int[9])*(m->data)
}
void test_transposeTrivial_1000()
{
    int * data = new int[1000 * 1000]{0};
    data[780] = 780;

    data[1002] = 6;
    data[2001] = 2001;

    Matrix * m = new Matrix(data, 1000);
    m->transpose(m->matrix);

    Debug::assert(m->data[780] == 0, __FUNCTION__);
    Debug::assert(m->data[780 * 1000] == 780, __FUNCTION__);
    Debug::assert(m->data[2001] == 6, __FUNCTION__);
    Debug::assert(m->data[1002] == 2001, __FUNCTION__);
}

void runTests()
{
    std::cerr << "Running tests." << std::endl;
    test_transpose_2(); std::cerr << ".";
    test_transpose_3(); std::cerr << ".";
    test_transpose_1000(); std::cerr << ".";
    test_transposeTrivial_3(); std::cerr << ".";
    test_transposeTrivial_1000(); std::cerr << ".";
    std::cerr << std::endl << "Tests done." << std::endl;
}




int main(int argc, char ** argv)
{
    //Debug::waitForKeyPressEnter(); //to be able to check for memory leaks in task manager
    runTests();
    //Debug::waitForKeyPressEnter();
    exit(0);
}

#endif