#!/bin/bash


print_time()
{
	MIN=$((SECONDS / 60))
	SEC=$((SECONDS - MIN * 60))
	echo "${MIN} min ${SEC} s"
}

if [ "$1" == "" ]; then
	echo "give program name as arg"
	exit
fi


#t tt
P=$1
echo "start"
for i in {1..10}
do
	./${P} > out/${P}1/${i}
	echo ${i};
done
print_time